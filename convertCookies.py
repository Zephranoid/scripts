#!/usr/bin/env python 

# Original code from here: https://productforums.google.com/d/msg/chrome/LWvfAFolOW4/hrC8ssNr27YJ
# Based On: https://gist.github.com/eestrada/6cc357602d3b31aaa933

"""
Converts cookies from qutebrowsers cookie database into 'Netscape' format
Error Codes:
    1: No output file Specified
    2: Incorrect argument input
    3: Incorrect format of call
"""

import os
import sqlite3
import io
import sys

def showHelp(argv=sys.argv):
    print("Usage: %s [options] <output_file>" % (argv[0]))
    print("Options:")
    print("\t-i <cookie_file>\tSpecifies input file")
    print("\t-d <num> <domains>\tSpecifies the domains to get cookies for (Seperate domains with space)")
    print("\t-h\t\t\tShow this help menu")

def main(argv=sys.argv, argc=len(sys.argv)):
    #Set default database
    cookie_file = os.environ['HOME'] + "/.local/share/qutebrowser/webengine/Cookies"

    output_file = None
    domains = []
    number_of_domains = 0
    skipCount = 0

    #Parse arguments
    for arg in range(1, argc):
        if skipCount:
            skipCount = skipCount - 1
        else:
            if argv[arg] == "-i":
                #Check for out of bounds error
                if arg + 1 == argc:
                    print("-i needs an input")
                    sys.exit(2)
                skipCount = 1
                cookie_file = argv[arg + 1]
            elif argv[arg] == "-d":
                number_of_domains = int(argv[arg + 1])
                skipCount = number_of_domains + 1
                if arg + 1 + number_of_domains > argc - 1:
                    print("Number of domains specified and number listed do not match")
                    sys.exit(2)
                for i in range(0, number_of_domains):
                    domains.append(argv[arg + 2 + i])
            elif argv[arg] == "-h":
                showHelp()
                sys.exit(0)
            else:
                #Check if last argument
                if arg != argc -1:
                    print("Incorrect usage: %s" % (argv[arg]))
                    showHelp()
                    sys.exit(3)
                output_file = argv[arg]

    #Check that output file was specified
    if output_file == None:
        print("No output file specified")
        sys.exit(1)

    #Connect to database
    conn = sqlite3.connect(cookie_file)
    cur = conn.cursor()

    query = 'SELECT host_key, path, is_secure, expires_utc, name, value FROM cookies'

    if number_of_domains:
        query += ' WHERE'
        for i in range(0, number_of_domains):
            if i != 0:
                query += ' OR'
            query += (' host_key = "' + domains[i] + '"')

    cur.execute(query)
    with io.open(output_file, 'w') as f:
        i = 0
        f.write("# HTTP Cookie File\n")

        for row in cur.fetchall():
          f.write("%s\tTRUE\t%s\t%s\t%d\t%s\t%s\n" % (row[0], row[1], str(bool(row[2])).upper(), row[3], str(row[4]), str(row[5])))
          i += 1

        #Print number of cookies converted
        print("%d rows written" % i)
    #Is f.close() best practice?
    # f.close()
    conn.close()

if __name__ == '__main__':
    main()
